//! A pointer type for heap allocation.
//!
//! `Box<T>`, casually referred to as a 'box', provides the simplest form of
//! heap allocation in Rust. Boxes provide ownership for this allocation, and
//! drop their contents when they go out of scope.
//!
//! # Examples
//!
//! Creating a box:
//!
//! ```
//! let x = Box::new(5);
//! ```
//!
//! Creating a recursive data structure:
//!
//! ```
//! #[derive(Debug)]
//! enum List<T> {
//!     Cons(T, Box<List<T>>),
//!     Nil,
//! }
//!
//! fn main() {
//! let list: List<i32> = List::Cons(1, Box::new(List::Cons(2,
//! Box::new(List::Nil))));
//!     println!("{:?}", list);
//! }
//! ```
//!
//! This will print `Cons(1, Cons(2, Nil))`.
//!
//! Recursive structures must be boxed, because if the definition of `Cons`
//! looked like this:
//!
//! ```rust,ignore
//! Cons(T, List<T>),
//! ```
//!
//! It wouldn't work. This is because the size of a `List` depends on how many
//! elements are in the list, and so we don't know how much memory to allocate
//! for a `Cons`. By introducing a `Box`, which has a defined size, we know how
//! big `Cons` needs to be.

// use heap;
// use raw_vec::RawVec;
// use str::from_boxed_utf8_unchecked;

use allocator::Allocator;

use core::any::Any;
use core::borrow;
use core::cmp::Ordering;
use core::convert::From;
use core::fmt;
use core::hash::{self, Hash};
use core::iter::FusedIterator;
use core::marker::{self, Unsize};
use core::mem;
use core::ops::{CoerceUnsized, Deref, DerefMut};
use core::ops::{InPlace, Place, Placer};
use core::ptr::{self, Unique};

/// A pointer type for heap allocation.
///
/// See the [module-level documentation](../../mem-alloc/boxed/index.html) for
/// more.
pub struct Box<A: Allocator, T: ?Sized> {
    allocator: A,
    uniq: Unique<T>,
}

pub type BoxResult<A, T> = Result<Box<A, T>, <A as Allocator>::Error>;

/// `IntermediateBox` represents uninitialized backing storage for `Box`.
///
/// FIXME (pnkfelix): Ideally we would just reuse `Box<T>` instead of
/// introducing a separate `IntermediateBox<T>`; but then you hit
/// issues when you e.g. attempt to destructure an instance of `Box`,
/// since it is a lang item and so it gets special handling by the
/// compiler.  Easier just to make this parallel type for now.
///
/// FIXME (pnkfelix): Currently the `box` protocol only supports
/// creating instances of sized types. This IntermediateBox is
/// designed to be forward-compatible with a future protocol that
/// supports creating instances of unsized types; that is why the type
/// parameter has the `?Sized` generalization marker, and is also why
/// this carries an explicit size. However, it probably does not need
/// to carry the explicit alignment; that is just a work-around for
/// the fact that the `align_of` intrinsic currently requires the
/// input type to be Sized (which I do not think is strictly
/// necessary).
pub struct IntermediateBox<A: Allocator, T: ?Sized> {
    allocator: A,
    ptr: *mut u8,
    size: usize,
    align: usize,
    marker: marker::PhantomData<*mut T>,
}

impl<A: Allocator, T> Place<T> for IntermediateBox<A, T> {
    fn pointer(&mut self) -> *mut T {
        self.ptr as *mut T
    }
}

unsafe fn finalize<A: Allocator, T>(b: IntermediateBox<A, T>) -> Box<A, T> {
    let uniq = Unique::new(b.ptr as *mut T);
    let allocator = ptr::read(&b.allocator);
    mem::forget(b);
    Box { allocator, uniq }
}

fn make_place<A: Allocator, T>(allocator: A)
    -> Result<IntermediateBox<A, T>, A::Error> {
    let size = mem::size_of::<T>();
    let align = mem::align_of::<T>();

    let p = if size == 0 {
        mem::align_of::<T>() as *mut u8
    } else {
        allocator.allocate(size, align)?
    };

    Ok(
        IntermediateBox {
            allocator: allocator,
            ptr: p,
            size: size,
            align: align,
            marker: marker::PhantomData,
        }
    )
}

impl<A: Allocator, T> Placer<T> for IntermediateBox<A, T> {
    type Place = Self;
    fn make_place(self) -> Self {
        self
    }
}

impl<A: Allocator, T> InPlace<T> for IntermediateBox<A, T> {
    type Owner = Box<A, T>;
    unsafe fn finalize(self) -> Box<A, T> {
        finalize(self)
    }
}

impl<A: Allocator, T: ?Sized> Drop for IntermediateBox<A, T> {
    fn drop(&mut self) {
        if self.size > 0 {
            unsafe {
                self.allocator.deallocate(self.ptr, self.size, self.align)
            }
        }
    }
}

impl<A: Allocator, T> Box<A, T> {
    /// Allocates memory on the heap and then places `x` into it.
    ///
    /// This doesn't actually allocate if `T` is zero-sized.
    ///
    /// # Examples
    ///
    /// ```
    /// let five = Box::new(5);
    /// ```
    #[inline(always)]
    pub fn new_in(allocator: A, x: T) -> BoxResult<A, T> {
        let place = make_place(allocator)?;
        Ok(place <- x)
    }
}

impl<A: Allocator + Default, T> Box<A, T> {
    #[inline(always)]
    pub fn new(x: T) -> BoxResult<A, T> {
        Box::new_in(A::default(), x)
    }
}

impl<A: Allocator, T: ?Sized> Box<A, T> {
    /// Constructs a box from a raw pointer.
    ///
    /// After calling this function, the raw pointer is owned by the
    /// resulting `Box`. Specifically, the `Box` destructor will call
    /// the destructor of `T` and free the allocated memory. Since the
    /// way `Box` allocates and releases memory is unspecified, the
    /// only valid pointer to pass to this function is the one taken
    /// from another `Box` via the [`Box::into_raw`] function.
    ///
    /// This function is unsafe because improper use may lead to
    /// memory problems. For example, a double-free may occur if the
    /// function is called twice on the same raw pointer.
    ///
    /// [`Box::into_raw`]: struct.Box.html#method.into_raw
    ///
    /// # Examples
    ///
    /// ```
    /// let x = Box::new(5);
    /// let ptr = Box::into_raw(x);
    /// let x = unsafe { Box::from_raw(ptr) };
    /// ```
    pub unsafe fn from_raw(allocator: A, raw: *mut T) -> Self {
        let uniq = Unique::new(raw);
        Box { allocator, uniq }
    }

    /// Consumes the `Box`, returning the wrapped raw pointer.
    ///
    /// After calling this function, the caller is responsible for the
    /// memory previously managed by the `Box`. In particular, the
    /// caller should properly destroy `T` and release the memory. The
    /// proper way to do so is to convert the raw pointer back into a
    /// `Box` with the [`Box::from_raw`] function.
    ///
    /// Note: this is an associated function, which means that you have
    /// to call it as `Box::into_raw(b)` instead of `b.into_raw()`. This
    /// is so that there is no conflict with a method on the inner type.
    ///
    /// [`Box::from_raw`]: struct.Box.html#method.from_raw
    ///
    /// # Examples
    ///
    /// ```
    /// let x = Box::new(5);
    /// let ptr = Box::into_raw(x);
    /// ```
    pub fn into_raw(mut b: Box<A, T>) -> (A, *mut T) {
        let allocator = unsafe { ptr::read(&b.allocator) };
        let ptr = unsafe { b.uniq.as_mut() as *mut T };
        mem::forget(b);
        (allocator, ptr)
    }
}

impl<A: Allocator, T: ?Sized> Drop for Box<A, T> {
    fn drop(&mut self) {
        let size = mem::size_of_val(&*self);
        let align = mem::align_of_val(&*self);
        unsafe {
            ptr::drop_in_place(&mut *self);
        }
        if size > 0 {
            unsafe {
                let ptr = self.deref_mut() as *mut T as *mut u8;
                self.allocator.deallocate(ptr, size, align);
            }
        }
    }
}

impl<A: Allocator + Default, T: Default> Default for Box<A, T>
    where A::Error: fmt::Debug
{
    /// Creates a `Box<A, T>`, with the `Default` value for T.
    fn default() -> Box<A, T> {
        Box::new(Default::default())
            .expect("Failed to allocate during default!")
    }
}

impl<A: Allocator + Default, T> Default for Box<A, [T]>
    where A::Error: fmt::Debug
{
    fn default() -> Box<A, [T]> {
        Box::<A, [T; 0]>::new([]).expect("Failed to allocate during default!")
    }
}

// impl<A: Allocator + Default> Default for Box<A, str>
//    where A::Error: fmt::Debug
//
//    fn default() -> Box<A, str> {
//        unsafe { from_boxed_utf8_unchecked(Default::default()) }
//    }
//

impl<A: Allocator + Clone, T: Clone> Clone for Box<A, T>
    where A::Error: fmt::Debug
{
    /// Returns a new box with a `clone()` of this box's contents.
    ///
    /// # Examples
    ///
    /// ```
    /// let x = Box::new(5);
    /// let y = x.clone();
    /// ```
    #[inline]
    fn clone(&self) -> Box<A, T> {
        let allocator = self.allocator.clone();
        Box::new_in(allocator, T::clone(&*self))
            .expect("Failed to allocate during clone!")
    }
    /// Copies `source`'s contents into `self` without creating a new
    /// allocation.
    ///
    /// # Examples
    ///
    /// ```
    /// let x = Box::new(5);
    /// let mut y = Box::new(10);
    ///
    /// y.clone_from(&x);
    ///
    /// assert_eq!(*y, 5);
    /// ```
    #[inline]
    fn clone_from(&mut self, source: &Box<A, T>) {
        (**self).clone_from(&(**source));
    }
}


// impl Clone for Box<A, str> {
//    fn clone(&self) -> Self {
//        let len = self.len();
//        let buf = RawVec::with_capacity(len);
//        unsafe {
//            ptr::copy_nonoverlapping(self.as_ptr(), buf.ptr(), len);
//            from_boxed_utf8_unchecked(buf.into_box())
//        }
//    }
//

impl<A: Allocator, T: ?Sized + PartialEq> PartialEq for Box<A, T> {
    #[inline]
    fn eq(&self, other: &Box<A, T>) -> bool {
        PartialEq::eq(&**self, &**other)
    }
    #[inline]
    fn ne(&self, other: &Box<A, T>) -> bool {
        PartialEq::ne(&**self, &**other)
    }
}

impl<A: Allocator, T: ?Sized + PartialOrd> PartialOrd for Box<A, T> {
    #[inline]
    fn partial_cmp(&self, other: &Box<A, T>) -> Option<Ordering> {
        PartialOrd::partial_cmp(&**self, &**other)
    }
    #[inline]
    fn lt(&self, other: &Box<A, T>) -> bool {
        PartialOrd::lt(&**self, &**other)
    }
    #[inline]
    fn le(&self, other: &Box<A, T>) -> bool {
        PartialOrd::le(&**self, &**other)
    }
    #[inline]
    fn ge(&self, other: &Box<A, T>) -> bool {
        PartialOrd::ge(&**self, &**other)
    }
    #[inline]
    fn gt(&self, other: &Box<A, T>) -> bool {
        PartialOrd::gt(&**self, &**other)
    }
}

impl<A: Allocator, T: ?Sized + Ord> Ord for Box<A, T> {
    #[inline]
    fn cmp(&self, other: &Box<A, T>) -> Ordering {
        Ord::cmp(&**self, &**other)
    }
}

impl<A: Allocator, T: ?Sized + Eq> Eq for Box<A, T> {}

impl<A: Allocator, T: ?Sized + Hash> Hash for Box<A, T> {
    fn hash<H: hash::Hasher>(&self, state: &mut H) {
        (**self).hash(state);
    }
}

impl<A: Allocator + Default, T> From<T> for Box<A, T>
    where A::Error: fmt::Debug
{
    fn from(t: T) -> Self {
        Box::new(t).expect("Failed to allocate during from!")
    }
}

// impl<'a, T: Copy> From<&'a [T]> for Box<A, [T]> {
//    fn from(slice: &'a [T]) -> Box<A, [T]> {
//        let mut boxed =
//            unsafe { RawVec::with_capacity(slice.len()).into_box() };
//        boxed.copy_from_slice(slice);
//        boxed
//    }
//

// impl<'a> From<&'a str> for Box<A, str> {
//    fn from(s: &'a str) -> Box<A, str> {
//        unsafe { from_boxed_utf8_unchecked(Box::from(s.as_bytes())) }
//    }
//

// impl From<Box<A, str>> for Box<A, [u8]> {
//    fn from(s: Box<A, str>) -> Self {
//        unsafe { mem::transmute(s) }
//    }
//

impl<A: Allocator> Box<A, Any> {
    #[inline]
    /// Attempt to downcast the box to a concrete type.
    ///
    /// # Examples
    ///
    /// ```
    /// use std::any::Any;
    ///
    /// fn print_if_string(value: Box<A, Any>) {
    ///     if let Ok(string) = value.downcast::<String>() {
    ///         println!("String ({}): {}", string.len(), string);
    ///     }
    /// }
    ///
    /// fn main() {
    ///     let my_string = "Hello World".to_string();
    ///     print_if_string(Box::new(my_string));
    ///     print_if_string(Box::new(0i8));
    /// }
    /// ```
    pub fn downcast<T: Any>(self) -> Result<Box<A, T>, Box<A, Any>> {
        if self.is::<T>() {
            let (allocator, raw) = Box::into_raw(self);
            unsafe { Ok(Box::from_raw(allocator, raw as *mut T)) }
        } else {
            Err(self)
        }
    }
}

impl<A: Allocator> Box<A, Any + Send> {
    #[inline]
    /// Attempt to downcast the box to a concrete type.
    ///
    /// # Examples
    ///
    /// ```
    /// use std::any::Any;
    ///
    /// fn print_if_string(value: Box<A, Any + Send>) {
    ///     if let Ok(string) = value.downcast::<String>() {
    ///         println!("String ({}): {}", string.len(), string);
    ///     }
    /// }
    ///
    /// fn main() {
    ///     let my_string = "Hello World".to_string();
    ///     print_if_string(Box::new(my_string));
    ///     print_if_string(Box::new(0i8));
    /// }
    /// ```
    pub fn downcast<T: Any>(self) -> Result<Box<A, T>, Box<A, Any + Send>> {
        if self.is::<T>() {
            let (allocator, raw) = Box::into_raw(self);
            unsafe { Ok(Box::from_raw(allocator, raw as *mut T)) }
        } else {
            Err(self)
        }
    }
}

impl<A: Allocator, T: fmt::Display + ?Sized> fmt::Display for Box<A, T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Display::fmt(&**self, f)
    }
}

impl<A: Allocator, T: fmt::Debug + ?Sized> fmt::Debug for Box<A, T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Debug::fmt(&**self, f)
    }
}

impl<A: Allocator, T: ?Sized> fmt::Pointer for Box<A, T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // It's not possible to extract the inner Uniq directly from the Box,
        // instead we cast it to a *const which aliases the Unique
        let ptr: *const T = &**self;
        fmt::Pointer::fmt(&ptr, f)
    }
}

impl<A: Allocator, T: ?Sized> Deref for Box<A, T> {
    type Target = T;

    fn deref(&self) -> &T {
        unsafe { self.uniq.as_ref() }
    }
}

impl<A: Allocator, T: ?Sized> DerefMut for Box<A, T> {
    fn deref_mut(&mut self) -> &mut T {
        unsafe { self.uniq.as_mut() }
    }
}

impl<A: Allocator, I: Iterator + ?Sized> Iterator for Box<A, I> {
    type Item = I::Item;
    fn next(&mut self) -> Option<I::Item> {
        (**self).next()
    }
    fn size_hint(&self) -> (usize, Option<usize>) {
        (**self).size_hint()
    }
    fn nth(&mut self, n: usize) -> Option<I::Item> {
        (**self).nth(n)
    }
}

impl<A: Allocator, I: DoubleEndedIterator + ?Sized> DoubleEndedIterator
    for Box<A, I> {
    fn next_back(&mut self) -> Option<I::Item> {
        (**self).next_back()
    }
}

impl<A: Allocator, I: ExactSizeIterator + ?Sized> ExactSizeIterator
    for Box<A, I> {
    fn len(&self) -> usize {
        (**self).len()
    }
    fn is_empty(&self) -> bool {
        (**self).is_empty()
    }
}

impl<A: Allocator, I: FusedIterator + ?Sized> FusedIterator for Box<A, I> {}

impl<A, T, U> CoerceUnsized<Box<A, U>> for Box<A, T>
    where A: Allocator,
          T: ?Sized + Unsize<U>,
          U: ?Sized
{
}

// impl<A: Allocator, T: Clone> Clone for Box<A, [T]> {
//    fn clone(&self) -> Self {
//        let mut new = BoxBuilder {
//            data: RawVec::with_capacity(self.len()),
//            len: 0,
//        };
//
//        let mut target = new.data.ptr();
//
//        for item in self.iter() {
//            unsafe {
//                ptr::write(target, item.clone());
//                target = target.offset(1);
//            };
//
//            new.len += 1;
//        }
//
//        return unsafe { new.into_box() };
//
//        // Helper type for responding to panics correctly.
//        struct BoxBuilder<T> {
//            data: RawVec<T>,
//            len: usize,
//        }
//
//        impl<T> BoxBuilder<T> {
//            unsafe fn into_box(self) -> Box<A, [T]> {
//                let raw = ptr::read(&self.data);
//                mem::forget(self);
//                raw.into_box()
//            }
//        }
//
//        impl<T> Drop for BoxBuilder<T> {
//            fn drop(&mut self) {
//                let mut data = self.data.ptr();
//                let max = unsafe { data.offset(self.len as isize) };
//
//                while data != max {
//                    unsafe {
//                        ptr::read(data);
//                        data = data.offset(1);
//                    }
//                }
//            }
//        }
//    }
//

impl<A: Allocator, T: ?Sized> borrow::Borrow<T> for Box<A, T> {
    fn borrow(&self) -> &T {
        &**self
    }
}

impl<A: Allocator, T: ?Sized> borrow::BorrowMut<T> for Box<A, T> {
    fn borrow_mut(&mut self) -> &mut T {
        &mut **self
    }
}

impl<A: Allocator, T: ?Sized> AsRef<T> for Box<A, T> {
    fn as_ref(&self) -> &T {
        &**self
    }
}

impl<A: Allocator, T: ?Sized> AsMut<T> for Box<A, T> {
    fn as_mut(&mut self) -> &mut T {
        &mut **self
    }
}


mod fn_once {
    #[repr(C)]
    pub struct TraitObject<A, R> {
        pub data: *mut u8,
        pub vtable: *mut VTable<A, R>,
    }

    #[repr(C)]
    #[derive(Copy, Clone)]
    pub struct VTable<A, R> {
        // destructor is called automatically, we can ignore it
        pub destructor: unsafe fn(),
        pub size: usize,
        pub align: usize,
        pub call_once: unsafe fn(A) -> R,
    }
}

impl<Alloc: Allocator, A, R> FnOnce<A> for Box<Alloc, FnOnce(A) -> R> {
    type Output = R;
    extern "rust-call" fn call_once(self, args: A) -> R {
        unsafe {
            /*
             * Since the "rust-call" ABI aliases to LLVM's "CCall" ABI,
             * we can use that to figure out how to pass arguments
             * and closure captures to our trait objects call_once
             * method.
             *
             * On x86, armv6, armv7, and aarch64, this means that
             * data of size less than a pointer is passed directly,
             * and data greater than a pointer is passed via pointer.
             */
            use self::fn_once::*;
            let (allocator, ptr) = Box::into_raw(self);
            let trait_object: TraitObject<A, R> = mem::transmute(ptr);
            let VTable {
                size,
                align,
                call_once,
                ..
            } = *trait_object.vtable;
            let data = trait_object.data;
            let result;
            if size > mem::size_of::<usize>() {
                // data is sent -via- ptr
                let call_once: fn(*mut u8, A) -> R = mem::transmute(call_once);
                result = call_once(data, args);
            } else if size > 0 {
                // data is sent -as- a pointer-sized value
                let call_once: fn(usize, A) -> R = mem::transmute(call_once);
                result = call_once(data as usize, args);
            } else {
                // size is 0, no data is sent
                let call_once: fn(A) -> R = mem::transmute(call_once);
                result = call_once(args);
            }
            if size > 0 {
                allocator.deallocate(ptr as *mut u8, size, align);
            }
            result
        }
    }
}
