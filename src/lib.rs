#![no_std]
#![feature(unique)]
#![feature(unboxed_closures)]
#![feature(placement_in_syntax)]
#![feature(placement_new_protocol)]
#![feature(coerce_unsized)]
#![feature(unsize)]
#![feature(fn_traits)]
#![feature(fused)]
#![feature(exact_size_is_empty)]

pub mod allocator;
pub mod boxed;
#[cfg(feature = "heap")]
pub mod heap;
pub mod slab;

pub mod prelude {
    pub use allocator::Allocator;
    pub use boxed::Box;
    #[cfg(feature = "heap")]
    pub use heap::HEAP;
}
