pub trait Allocator {
    type Error;

    fn allocate(
        &self,
        size: usize,
        align: usize,
    ) -> Result<*mut u8, Self::Error>;

    unsafe fn deallocate(&self, ptr: *mut u8, old_size: usize, align: usize);

    unsafe fn reallocate(
        &self,
        ptr: *mut u8,
        old_size: usize,
        size: usize,
        align: usize,
    ) -> Result<*mut u8, Self::Error>;

    unsafe fn reallocate_inplace(
        &self,
        ptr: *mut u8,
        old_size: usize,
        size: usize,
        align: usize,
    ) -> Result<usize, Self::Error>;

    fn usable_size(
        &self,
        size: usize,
        align: usize,
    ) -> Result<usize, Self::Error>;
}

impl<'a, A: Allocator + ?Sized + 'a> Allocator for &'a A {
    type Error = A::Error;

    fn allocate(
        &self,
        size: usize,
        align: usize,
    ) -> Result<*mut u8, Self::Error> {
        (&**self).allocate(size, align)
    }

    unsafe fn deallocate(&self, ptr: *mut u8, old_size: usize, align: usize) {
        (&**self).deallocate(ptr, old_size, align);
    }

    unsafe fn reallocate(
        &self,
        ptr: *mut u8,
        old_size: usize,
        size: usize,
        align: usize,
    ) -> Result<*mut u8, Self::Error> {
        (&**self).reallocate(ptr, old_size, size, align)
    }

    unsafe fn reallocate_inplace(
        &self,
        ptr: *mut u8,
        old_size: usize,
        size: usize,
        align: usize,
    ) -> Result<usize, Self::Error> {
        (&**self).reallocate_inplace(ptr, old_size, size, align)
    }

    fn usable_size(
        &self,
        size: usize,
        align: usize,
    ) -> Result<usize, Self::Error> {
        (&**self).usable_size(size, align)
    }
}
