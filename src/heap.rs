use allocator::Allocator;

#[allow(improper_ctypes)]
extern "C" {
    fn __rust_allocate(size: usize, align: usize) -> *mut u8;
    fn __rust_deallocate(ptr: *mut u8, old_size: usize, align: usize);
    fn __rust_reallocate(
        ptr: *mut u8,
        old_size: usize,
        size: usize,
        align: usize,
    ) -> *mut u8;
    fn __rust_reallocate_inplace(
        ptr: *mut u8,
        old_size: usize,
        size: usize,
        align: usize,
    ) -> usize;
    fn __rust_usable_size(size: usize, align: usize) -> usize;
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Error {
    InvalidAlignment(usize),
    SizeCannotBeZero,
    SizeTooLarge(usize),
    OutOfMemory,
}

fn check_size_and_alignment(size: usize, align: usize) -> Result<(), Error> {
    use self::Error::*;
    if size == 0 {
        return Err(SizeCannotBeZero);
    }
    if size > isize::max_value() as usize {
        return Err(SizeTooLarge(size));
    }
    if !usize::is_power_of_two(align) {
        return Err(InvalidAlignment(align));
    }
    Ok(())
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Heap {}

#[allow(unused)]
pub static HEAP: Heap = Heap {};

unsafe impl Send for Heap {}
unsafe impl Sync for Heap {}

impl Default for Heap {
    fn default() -> Heap {
        HEAP
    }
}

impl Allocator for Heap {
    type Error = self::Error;
    fn allocate(
        &self,
        size: usize,
        align: usize,
    ) -> Result<*mut u8, Self::Error> {
        let _ = check_size_and_alignment(size, align)?;
        let ptr = unsafe { __rust_allocate(size, align) };
        if ptr.is_null() {
            return Err(Error::OutOfMemory);
        }
        Ok(ptr)
    }
    unsafe fn deallocate(&self, ptr: *mut u8, old_size: usize, align: usize) {
        __rust_deallocate(ptr, old_size, align);
    }
    unsafe fn reallocate(
        &self,
        ptr: *mut u8,
        old_size: usize,
        size: usize,
        align: usize,
    ) -> Result<*mut u8, Self::Error> {
        let _ = check_size_and_alignment(size, align)?;
        let ptr = __rust_reallocate(ptr, old_size, size, align);
        if ptr.is_null() {
            return Err(Error::OutOfMemory);
        }
        Ok(ptr)
    }
    unsafe fn reallocate_inplace(
        &self,
        ptr: *mut u8,
        old_size: usize,
        size: usize,
        align: usize,
    ) -> Result<usize, Self::Error> {
        let _ = check_size_and_alignment(size, align)?;
        let actual_size = __rust_reallocate_inplace(ptr, old_size, size, align);
        Ok(actual_size)
    }
    fn usable_size(
        &self,
        size: usize,
        align: usize,
    ) -> Result<usize, Self::Error> {
        let _ = check_size_and_alignment(size, align)?;
        let size = unsafe { __rust_usable_size(size, align) };
        Ok(size)
    }
}
