use allocator::Allocator;
use core::sync::atomic::{AtomicUsize, Ordering};

pub type SLAB = &'static Slab;

#[derive(Debug)]
pub struct Slab {
    start: *mut u8,
    size: usize,
    pos: AtomicUsize,
}

#[derive(Debug, Copy, Clone)]
pub enum Error {
    SizeCannotBeZero,
    OutOfMemory,
    ArithmeticError,
    AlignmentCannotBeZero,
    AlignmentMustBePowerOfTwo,
}

use self::Error::*;

type Result<A> = ::core::result::Result<A, Error>;

impl Slab {
    pub unsafe fn new_from_raw(start: *mut u8, size: usize) -> Slab {
        let pos = AtomicUsize::new(0);
        Slab { start, size, pos }
    }

    fn start(&self) -> usize {
        self.start as usize
    }

    fn end(&self) -> usize {
        self.start() + self.size
    }

    fn cur_addr(&self) -> usize {
        self.start() + self.pos.load(Ordering::SeqCst)
    }
}


fn next_aligned(cur_addr: usize, align: usize) -> Result<usize> {
    if !align.is_power_of_two() {
        return Err(AlignmentMustBePowerOfTwo);
    }
    if align == 0 {
        return Err(AlignmentCannotBeZero);
    }
    assert!(align.is_power_of_two());
    assert!(align > 0);
    let mask = align - 1;
    let offset = cur_addr & mask;
    let result;
    if offset != 0 {
        result = cur_addr - offset + align;
    } else {
        result = cur_addr;
    }
    Ok(result)
}

impl Allocator for Slab {
    type Error = self::Error;

    fn allocate(&self, size: usize, align: usize) -> Result<*mut u8> {
        if size == 0 {
            return Err(SizeCannotBeZero);
        }
        loop {
            let cur_addr = self.cur_addr();
            let smallest_aligned = next_aligned(cur_addr, align)?;
            let alloc_end =
                smallest_aligned.checked_add(size).ok_or(ArithmeticError)?;
            if alloc_end >= self.end() {
                return Err(OutOfMemory);
            }
            let old_pos = cur_addr - self.start();
            let new_pos = alloc_end - self.start();
            if let Ok(_) = self.pos
                   .compare_exchange(
                old_pos,
                new_pos,
                Ordering::SeqCst,
                Ordering::SeqCst,
            ) {
                let alloc_start = alloc_end - size;
                return Ok(alloc_start as *mut u8);
            }
        }
    }

    #[allow(unused_variables)]
    unsafe fn deallocate(&self, ptr: *mut u8, size: usize, align: usize) {}

    unsafe fn reallocate(
        &self,
        ptr: *mut u8,
        old_size: usize,
        size: usize,
        align: usize,
    ) -> Result<*mut u8> {
        if size <= old_size {
            return Ok(ptr);
        }
        self.allocate(size, align)
    }

    #[allow(unused_variables)]
    unsafe fn reallocate_inplace(
        &self,
        ptr: *mut u8,
        old_size: usize,
        size: usize,
        align: usize,
    ) -> Result<usize> {
        if size <= old_size {
            self.usable_size(size, align)
        } else {
            self.usable_size(old_size, align)
        }
    }

    #[allow(unused_variables)]
    fn usable_size(&self, size: usize, align: usize) -> Result<usize> {
        Ok(size)
    }
}
